'use strict'

/*
 * Application Public Interface
 * ----------------------------
 * 1.- Modules and dependencies
 * 2.- Libraries
 * 3.- Local variables
 */
const Http = require('http')

const HttpHandler = require('../lib/http-handler')

const port = process.env.PORT || 8081

/*
 * Local config
 */
HttpHandler.get('/', handleRequest)
HttpHandler.get('/users', handleRequest)
HttpHandler.post('/users', handleRequest)

/*
 * Magic happens
 */
Http
	.createServer(HttpHandler.serve)
	.listen(port, onListening)
	
/*
 * Helpers
 */
function handleRequest (req, res) {
	res.end(`${req.method} ${req.url}\n`)
}

function onListening () {
	console.log(`Server running at localhost:${port}`)
}