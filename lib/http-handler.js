'use strict'

var HttpHandler = (function (){
	let stack = []
	
	function use (action, route, fn) {
		let rest = {}
		
		let method = action
		let path = route
		let handler = fn
		
		rest.method = method
		rest.path = path
		rest.handler = handler
		
		stack.push(rest)
	}
	
	function get (route, fn) {
		use('GET', route, fn)
	}
	
	function post (route, fn) {
		use('POST', route, fn)
	}
	
	function put (route, fn) {
		use('PUT', route, fn)
	}
	
	function del (route, fn) {
		use('DELETE', route, fn)
	}

	function handleGet (req, res) {
		let len = stack.length
		
		for(let route = 0; route < len; route++) {
			if (req.url == stack[route].path) {
				res.writeHead(200, { 'Content-Type' : 'text/plain' })
				res.statusMessage = 'OK'
				return stack[route].handler(req, res)
			}
		}
		
		unexistingRequest(req, res)
	}

	function handlePost (req, res) {
		let len = stack.length
		
		for(let route = 0; route < len; route++) {
			if (req.url == stack[route].path) {
				res.writeHead(200, { 'Content-Type' : 'text/plain' })
				res.statusMessage = 'OK'
				return stack[route].handler(req, res)
			}
		}
		
		unexistingRequest(req, res)
	}
	
	function handlePut (req, res) {
		let len = stack.length
		
		for(let route = 0; route < len; route++) {
			if (req.url == stack[route].path) {
				res.writeHead(200, { 'Content-Type' : 'text/plain' })
				res.statusMessage = 'OK'
				return stack[route].handler(req, res)
			}
		}
		
		return unexistingRequest(req, res)
	}
	
	function handleDelete (req, res) {
		let len = stack.length
		
		for(let route = 0; route < len; route++) {
			if (req.url == stack[route].path) {
				res.writeHead(200, { 'Content-Type' : 'text/plain' })
				res.statusMessage = 'OK'
				return stack[route].handler(req, res)
			}
		}
		
		unexistingRequest(req, res)
	}

	function unexistingRequest(req, res) {
		res.writeHead(404, { 'Content-Type' : 'text/plain'})
		res.statusMessage = 'Not Found'
		res.write('404 Not Found :(\n')
		res.end()
	}

	function badRequest (req, res) {
		res.writeHead(400, { 'Content-Type' : 'text/plain' })
		res.statusMessage = '400 Bad Request'
		res.write('400 Bad Request :(\n')
		res.end()
	}
	
	function serve (req, res) {
		switch (req.method) {
			case 'GET':
				handleGet(req, res)
				break;
			case 'POST':
				handlePost(req, res)
				break;
			case 'PUT':
				handlePut(req, res)
				break;
			case 'DELETE':
				handleDelete(req, res)
				break;
			default:
				badRequest(req, res)
				break;
		}
		
		console.log(`${req.method} ${req.url} ${res.statusCode} ${res.statusMessage}`)
	}
	
	return {
		serve : serve,
		get : get,
		post : post,
		put : put,
		del : del,
		use : use
	}
})()

module.exports = HttpHandler