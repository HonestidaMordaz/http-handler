# Simple REST-API built on top of iojs (node)

# How-to use
	git clone https://github.com/MichelAyala/node-rest`  
	cd node-rest`  
	[sudo] npm install

`(index|app|server).js`  
  
	/* Modules and dependencies */
	const Http = require('http')
	const HttpHandler = require('./lib/http-handler')

	const port = process.env.PORT || 8080

	/* Local config */
	HttpHandler.get('/', function (req, res) {
		res.end('Hello from /')
	})

	HttpHandler.get('/users', function (req, res) {
		res.end('Hello from /users')
	})

	/* Magic happens */
	Http
	  .createServer(HttpHandler.serve)
	  .listen(port)
